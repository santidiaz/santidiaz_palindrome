# SantidiazPalindrome

`santidiaz_palindrome` is a sample Ruby gem created in [*Learn Enough Ruby to Be Dangerous*](https://www.learnenough.com/ruby-tutorial) by Santiago Díaz.

## Installation

To install `santidiaz_palindrome`, add this line to your application's `Gemfile`:

```ruby
gem 'santidiaz_palindrome'
```

Then install as follows:

```
$ bundle install
```

Or install it directly using `gem`:

```
$ gem install santidiaz_palindrome
```

## Usage

`santidiaz_palindrome` adds a `palindrome?` method to the `String` class, and can be used as follows:

```
$ irb
>> require 'santidiaz_palindrome'
>> "Boca Juniors".palindrome?
=> false
>> "racecar".palindrome?
=> true
>> "Arriba la birra".palindrome?
=> true
>> phrase = "Madam, I'm Adam."
>> phrase.palindrome?
=> true
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).